package cook.impl;

import cook.Cook;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class Stew implements Cook {
    @Override
    public void cookMethod() {
        System.out.println("开始炖");
    }
}
