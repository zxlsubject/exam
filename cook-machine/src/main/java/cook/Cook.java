package cook;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public interface Cook {
    public void cookMethod();
}
