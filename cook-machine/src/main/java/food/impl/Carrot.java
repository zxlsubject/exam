package food.impl;

import food.Food;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class Carrot implements Food {
    @Override
    public void foodKind() {
        System.out.println("主料：胡萝卜");
    }
}
