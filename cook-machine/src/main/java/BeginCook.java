import menu.impl.FishFlavored;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class BeginCook {
    public static void main(String[] args) {
        new FishFlavored().make();
    }
}
