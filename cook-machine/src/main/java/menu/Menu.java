package menu;

import step.common.Step;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public interface Menu {
    public void make();
}
