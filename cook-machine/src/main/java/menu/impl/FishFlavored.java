package menu.impl;

import cook.impl.Fry;
import food.impl.Carrot;
import food.impl.Fungus;
import food.impl.Meat;
import ingredient.impl.Garlic;
import ingredient.impl.Ginger;
import ingredient.impl.Onion;
import menu.Menu;
import step.operation.DishUp;
import step.operation.PourOil;
import step.food.StepFood;
import step.ingredient.StepIngredient;
import step.operation.Thicken;
import step.cook.StepCook;
import step.common.StepInit;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class FishFlavored implements Menu {
    private static final String name = "菜名：鱼香肉丝";

    @Override
    public void make() {
        new DishUp(
                new Thicken(
                        new StepCook(
                                new StepFood(
                                        new StepIngredient(
                                                new PourOil(new StepInit(name)), new Garlic(),new Ginger(),new Onion()
                                        ), new Meat(), new Carrot(), new Fungus()
                                ), new Fry()
                        )
                )
        ).operation();
    }
}
