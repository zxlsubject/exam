package ingredient.impl;

import ingredient.Ingredient;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class Onion implements Ingredient {
    @Override
    public void IngredientKind() {
        System.out.println("配料：葱");
    }
}
