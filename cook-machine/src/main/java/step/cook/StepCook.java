package step.cook;

import cook.Cook;
import step.common.Step;
import step.common.StepDec;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class StepCook extends StepDec {
    private Cook cook;

    public StepCook(Step step, Cook cook) {
        super(step);
        this.cook = cook;
    }
    @Override
    public void operation() {
        super.operation();
        cook.cookMethod();
    }
}
