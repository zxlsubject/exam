package step.common;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public abstract class Step {
    public abstract void operation();
}
