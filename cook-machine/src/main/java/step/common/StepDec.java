package step.common;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public abstract class StepDec extends Step {
    private Step step = null;

    public StepDec(Step step) {
        this.step = step;
    }
    @Override
    public void operation() {
        if(step != null) {
            this.step.operation();
        }
    }
}
