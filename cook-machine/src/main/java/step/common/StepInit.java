package step.common;


/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class StepInit extends Step{
    private String name;

    public StepInit(String name) {
        this.name = name;
    }
    @Override
    public void operation() {
        System.out.println(name);
    }
}
