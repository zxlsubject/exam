package step.ingredient;

import ingredient.Ingredient;
import java.util.Arrays;
import step.common.Step;
import step.common.StepDec;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class StepIngredient extends StepDec {
    private Ingredient[] ingredient;

    public StepIngredient(Step step, Ingredient...ingredient) {
        super(step);
        this.ingredient = ingredient;
    }
    @Override
    public void operation() {
        super.operation();
        Arrays.asList(ingredient).forEach(i -> i.IngredientKind());
    }
}
