package step.operation;

import step.common.Step;
import step.common.StepDec;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class PourOil extends StepDec {

    public PourOil(Step step) {
        super(step);
    }

    private void method() {
        System.out.println("倒油");
    }

    @Override
    public void operation() {
        super.operation();
        this.method();
    }
}
