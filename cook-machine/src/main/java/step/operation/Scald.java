package step.operation;

import step.common.Step;
import step.common.StepDec;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class Scald extends StepDec {

    public Scald(Step step) {
        super(step);
    }

    private void method() {
        System.out.println("主料焯水");
    }

    @Override
    public void operation() {
        super.operation();
        this.method();
    }
}
