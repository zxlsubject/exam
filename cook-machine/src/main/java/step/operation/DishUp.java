package step.operation;

import step.common.Step;
import step.common.StepDec;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class DishUp extends StepDec {

    public DishUp(Step step) {
        super(step);
    }

    private void method() {
        System.out.println("起锅装盘");
    }

    @Override
    public void operation() {
        super.operation();
        this.method();
    }
}
