package step.operation;

import step.common.Step;
import step.common.StepDec;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class Thicken extends StepDec {

    public Thicken(Step step) {
        super(step);
    }

    private void method() {
        System.out.println("勾芡");
    }

    @Override
    public void operation() {
        super.operation();
        this.method();
    }
}
