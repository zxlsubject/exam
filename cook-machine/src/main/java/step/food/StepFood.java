package step.food;

import food.Food;
import java.util.Arrays;
import step.common.Step;
import step.common.StepDec;

/**
 * @description:
 * @author: zhangxinlong
 * @create: 2020-09-27
 **/
public class StepFood extends StepDec {
    private Food[] food;

    public StepFood(Step step, Food...food) {
        super(step);
        this.food = food;
    }

    @Override
    public void operation() {
        super.operation();
        Arrays.asList(food).forEach(f -> f.foodKind());
    }
}
